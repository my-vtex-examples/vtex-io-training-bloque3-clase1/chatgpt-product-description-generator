import { json } from 'co-body'

export async function createMessage(ctx: Context, next: () => Promise<any>) {
  const {
    req,
    clients: { catalog },
  } = ctx

  const body = await json(req)

  const product = await catalog.getProductById(body?.productId)

  const TEMPLATE = `Actúa como si fueras un experto en copywriter, experto en marketing digital y un experto en comercio electrónico. Quiero que me generes la mejor descripción para el producto llamado "${product?.Name}". El texto debe estar optimizado para SEO Web y debe ser lo más amigable posible para los clientes de nuestro e-Commerce. Usa un tono serio pero amistoso. La descripción debe tener un máximo de 200 caracteres.`

  ctx.state.text = TEMPLATE
  // ctx.body = product
  await next()
}

export async function sendMessage(ctx: Context, next: () => Promise<any>) {
  const {
    state: { text },
    clients: { chatgpt },
  } = ctx

  const response = await chatgpt.getDescription(text)

  ctx.body = response
  await next()
}
